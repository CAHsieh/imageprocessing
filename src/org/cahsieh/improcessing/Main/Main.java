package org.cahsieh.improcessing.Main;

import org.cahsieh.improcessing.method.ImageProcess;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by CANCHU on 2016/5/24.
 */
public class Main {

    private static final String motherPath = "E:\\image\\";
    private static final String[] mFileName = new String[]{"cheer1", "cheer2", "cheer3", "cheer4", "cheer5", "lena", "wu"};

    private Main() {
    }

    private BufferedImage loadImage(String subPath) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(new BufferedInputStream(new FileInputStream(new File(motherPath + subPath))));
        } catch (IOException e) {
            e.printStackTrace();
            image = null;
        } finally {
            return image;
        }
    }

    public static void main(String[] args) {
        Main instance = new Main();
//        String filename = "cheer1";
        for (String filename : mFileName) {
            BufferedImage image = instance.loadImage(filename + ".bmp");
            ImageProcess process = new ImageProcess(image);
            process.makeGray();
//            process.outputGrayImage("gray\\" + filename + "_gray.bmp");

//            process.makeGammaEqualization(1);
//            process.outputGammaEqualizationImage("gamma_equalization_1\\" + filename + "_gray_gammaEq_1.bmp");
//
            process.makeGammaEqualization(0.5);
            process.outputGammaEqualizationImage("gamma_equalization_0.5\\" + filename + "_gray_gammaEq_0.5.bmp");

            process.addSaltAndPepper();
            process.outputSaltAndPepperImage("Salt_And_Pepper\\" + filename + "_gray_gammaEq_0.5_salt_and_pepper.bmp");

//            process.doMeanFilter();
//            process.outputMeanFilterImage("MeanFilter\\" + filename + "_gray_gammaEq_0.5_salt_and_pepper_MeanFilter.bmp");
            process.doMedianFilter();
            process.outputMedianFilterImage("MedianFilter\\" + filename + "_gray_gammaEq_0.5_salt_and_pepper_MeanFilter.bmp");

//            process.makeGammaEqualization(3);
//            process.outputGammaEqualizationImage("gamma_equalization_3\\" + filename + "_gray_gammaEq_3.bmp");

//            process.makeThreshold();
//            process.outputThresholdImage("threshold\\" + filename + "_gray_gammaEq_3_threshold.bmp");
        }
    }
}
