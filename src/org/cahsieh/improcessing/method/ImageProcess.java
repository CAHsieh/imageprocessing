package org.cahsieh.improcessing.method;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

/**
 * Created by CANCHU on 2016/5/24.
 */
public class ImageProcess {
    private static final String motherPath = "E:\\image\\";
    private BufferedImage image;
    private BufferedImage grayImage;
    private BufferedImage grayImageGammaEqualization;
    private BufferedImage grayImageThreshold;
    private BufferedImage grayImageSaltAndPepper;
    private BufferedImage grayImageMeanFilter;
    private BufferedImage grayImageMedianFilter;

    public ImageProcess(BufferedImage image) {
        this.image = image;
    }

    public void makeGray() {
        if (image == null) {
            System.out.println("origin image is null.");
            return;
        }
        ImageFilter grayFilter = new GrayFilter(false, 0);
        ImageProducer producer = new FilteredImageSource(image.getSource(), grayFilter);
        Image mImage = Toolkit.getDefaultToolkit().createImage(producer);
        if (mImage == null) {
            System.out.println("something error.");
            return;
        }
        grayImage = new BufferedImage(mImage.getWidth(null), mImage.getHeight(null), BufferedImage.TYPE_BYTE_GRAY);
        Graphics2D graph = grayImage.createGraphics();
        graph.drawImage(mImage, 0, 0, null);
        graph.dispose();
    }

    public void outputGrayImage(String subPath) {
        if (grayImage == null) {
            System.out.println("gray image does not create yet.");
            return;
        }
        try {
            ImageIO.write(grayImage, "bmp", new BufferedOutputStream(new FileOutputStream(new File(motherPath + subPath))));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("something error.");
        }
    }

    public void makeGammaEqualization(double gamma) {
        if (grayImage == null) {
            return;
        }
        int maxGray = 0;
        int minGray = 255;
        byte[] b = ((DataBufferByte) grayImage.getRaster().getDataBuffer()).getData();
        int[] myPixels = new int[b.length];
        for (int i = 0; i < b.length; i++) {
            myPixels[i] = b[i] & 0xFF;
            if (myPixels[i] > maxGray)
                maxGray = myPixels[i];
            if (myPixels[i] < minGray)
                minGray = myPixels[i];
        }

        double d = maxGray - minGray;
        for (int i = 0; i < myPixels.length; i++) {
            myPixels[i] = (int) (Math.pow(((double) (myPixels[i] - minGray) / d), gamma) * 255);
        }
        grayImageGammaEqualization = new BufferedImage(grayImage.getWidth(), grayImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        grayImageGammaEqualization.getRaster().setPixels(0, 0, grayImage.getWidth(), grayImage.getHeight(), myPixels);
    }

    public void outputGammaEqualizationImage(String subPath) {
        if (grayImageGammaEqualization == null) {
            System.out.println("gray image does not create yet.");
            return;
        }
        try {
            ImageIO.write(grayImageGammaEqualization, "bmp", new BufferedOutputStream(new FileOutputStream(new File(motherPath + subPath))));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("something error.");
        }
    }

    public void makeThreshold() {
        if (grayImageGammaEqualization == null) {
            return;
        }
        int Threshold = 0;
        byte[] b = ((DataBufferByte) grayImageGammaEqualization.getRaster().getDataBuffer()).getData();
        int[] myPixels = new int[b.length];
        for (int i = 0; i < b.length; i++) {
            myPixels[i] = b[i] & 0xFF;
            Threshold += myPixels[i];
        }
        Threshold /= myPixels.length;
        for (int i = 0; i < myPixels.length; i++) {
            myPixels[i] = myPixels[i] >= Threshold ? 255 : 0;
        }
        grayImageThreshold = new BufferedImage(grayImage.getWidth(), grayImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        grayImageThreshold.getRaster().setPixels(0, 0, grayImage.getWidth(), grayImage.getHeight(), myPixels);
    }

    public void outputThresholdImage(String subPath) {
        if (grayImageThreshold == null) {
            System.out.println("Threshold image does not create yet.");
            return;
        }
        try {
            ImageIO.write(grayImageThreshold, "bmp", new BufferedOutputStream(new FileOutputStream(new File(motherPath + subPath))));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("something error.");
        }
    }

    public void addSaltAndPepper() {
        if (grayImageGammaEqualization == null) {
            return;
        }
        byte[] b = ((DataBufferByte) grayImageGammaEqualization.getRaster().getDataBuffer()).getData();
        int[] myPixels = new int[b.length];
        Random random = new Random();
        for (int i = 0; i < b.length; i++) {
            myPixels[i] = b[i] & 0xFF;
            int task = random.nextInt(20);
            switch (task) {
                case 0:
                    myPixels[i] = 0;
                    break;
                case 1:
                    myPixels[i] = 255;
                    break;
            }
        }
        grayImageSaltAndPepper = new BufferedImage(grayImage.getWidth(), grayImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        grayImageSaltAndPepper.getRaster().setPixels(0, 0, grayImage.getWidth(), grayImage.getHeight(), myPixels);
    }

    public void outputSaltAndPepperImage(String subPath) {
        if (grayImageSaltAndPepper == null) {
            System.out.println("Salt And Pepper image does not create yet.");
            return;
        }
        try {
            ImageIO.write(grayImageSaltAndPepper, "bmp", new BufferedOutputStream(new FileOutputStream(new File(motherPath + subPath))));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("something error.");
        }
    }

    public void doMeanFilter() {
        if (grayImageSaltAndPepper == null) {
            return;
        }
        byte[] b = ((DataBufferByte) grayImageSaltAndPepper.getRaster().getDataBuffer()).getData();
        int[] myPixels = new int[b.length];
        for (int i = 0; i < b.length; i++)
            myPixels[i] = b[i] & 0xFF;
        int width = grayImageSaltAndPepper.getWidth();
        int height = grayImageSaltAndPepper.getHeight();
        for (int n = 0; n < 20; n++) {
            for (int i = 1; i < width - 1; i++) {
                for (int j = 1; j < height - 1; j++) {
                    int sum = 0;
                    for (int di = -1; di < 2; di++) {
                        for (int dj = -1; dj < 2; dj++) {
                            sum += myPixels[width * (j + dj) + (i + di)];
                        }
                    }
                    myPixels[width * j + i] = sum / 9;
                }
            }
        }
        grayImageMeanFilter = new BufferedImage(grayImage.getWidth(), grayImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        grayImageMeanFilter.getRaster().setPixels(0, 0, width, height, myPixels);
    }

    public void outputMeanFilterImage(String subPath) {
        if (grayImageMeanFilter == null) {
            System.out.println("MeanFilter image does not create yet.");
            return;
        }
        try {
            ImageIO.write(grayImageMeanFilter, "bmp", new BufferedOutputStream(new FileOutputStream(new File(motherPath + subPath))));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("something error.");
        }
    }

    public void doMedianFilter() {
        if (grayImageSaltAndPepper == null) {
            return;
        }
        byte[] b = ((DataBufferByte) grayImageSaltAndPepper.getRaster().getDataBuffer()).getData();
        int[] myPixels = new int[b.length];
        int[] newPixels = new int[b.length];
        for (int i = 0; i < b.length; i++)
            myPixels[i] = b[i] & 0xFF;
        int width = grayImageSaltAndPepper.getWidth();
        int height = grayImageSaltAndPepper.getHeight();
        for (int i = 1; i < width - 1; i++) {
            for (int j = 1; j < height - 1; j++) {
                int[] nine_value = new int[256];
                for (int di = -1; di < 2; di++) {
                    for (int dj = -1; dj < 2; dj++) {
                        nine_value[myPixels[width * (j + dj) + (i + di)]]++;
                    }
                }
                int sum = 0;
                for (int n = 0; n < 256; n++) {
                    sum += nine_value[n];
                    if (sum >= 5) {
                        newPixels[width * j + i] = n;
                        break;
                    }
                }
            }
        }
        grayImageMedianFilter = new BufferedImage(grayImage.getWidth(), grayImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        grayImageMedianFilter.getRaster().setPixels(0, 0, width, height, newPixels);
    }

    public void outputMedianFilterImage(String subPath) {
        if (grayImageMedianFilter == null) {
            System.out.println("MedianFilter image does not create yet.");
            return;
        }
        try {
            ImageIO.write(grayImageMedianFilter, "bmp", new BufferedOutputStream(new FileOutputStream(new File(motherPath + subPath))));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("something error.");
        }
    }
}
